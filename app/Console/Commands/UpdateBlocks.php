<?php

namespace App\Console\Commands;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Console\Command;


class UpdateBlocks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blocks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coin = $this->coin= new Evos();
        try{
            $info = $coin->getInfo();
            if(isset($info['error'])){
                $this->info("RPC gave error! ". $info['error']);
                return;
            }
        }catch (\Exception $e){
            $this->info("Deamon is not working");
            return;
        }

        return $this->updateBlocks();


    }

    public function updateBlocks(){
       $blocks = Block::get(['id','time','created_at']);
        $bar = $this->output->createProgressBar($blocks->count());
       foreach ($blocks as $key=>$block){
           $block->created_at = date("Y-m-d H:i:s",$block->time);
           $block->save();
           $bar->setProgress($key+1);
       }

    }
}
