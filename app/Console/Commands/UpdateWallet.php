<?php

namespace App\Console\Commands;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Console\Command;


class UpdateWallet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coin = $this->coin= new Evos();
        try{
            $info = $coin->getInfo();
            if(isset($info['error'])){
                $this->info("RPC gave error! ". $info['error']);
                return;
            }
        }catch (\Exception $e){
            $this->info("Deamon is not working");
            return;
        }

        return $this->updateWallets();


    }

    public function updateWallets(){
       $wallets = Wallet::get();
        $bar = $this->output->createProgressBar($wallets->count());
       foreach ($wallets as $key=>$wallet){
           $out = $wallet->transactions->where('way','out')->sum('value');
           $in = $wallet->transactions->where('way','in')->sum('value');
           $wallet->balance = $out-$in;
           $wallet->save();
           $bar->setProgress($key+1);
       }

    }
}
