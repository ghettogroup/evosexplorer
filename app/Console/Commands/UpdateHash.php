<?php

namespace App\Console\Commands;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\Transaction;
use Illuminate\Console\Command;


class UpdateHash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coin = $this->coin= new Evos();
        try{
            $info = $coin->getInfo();
            if(isset($info['error'])){
                $this->info("RPC gave error! ". $info['error']);
                return;
            }
        }catch (\Exception $e){
            $this->info("Deamon is not working");
            return;
        }

        return $this->updateBlocks();




        $maxBlock = $info['blocks'];
        $lastBlock = Block::orderBy('height','desc')->first(['height']);
        $syncedBlock = 1;
        $bar = $this->output->createProgressBar($maxBlock);
        if($lastBlock){
            $syncedBlock = $lastBlock['height']+1;
        }
        while ($syncedBlock <= $maxBlock){
            $bar->setProgress($syncedBlock);
            $coin->syncBlock($syncedBlock);
            $syncedBlock++;
        }
        dd($syncedBlock);

        dd($info);

    }

    public function updateBlocks(){
        $blocks = Block::get();

        $count = $blocks->count();
        $bar = $this->output->createProgressBar($count);
        $i =1;
        foreach ($blocks as $block){
            $bar->setProgress($i);

            $block_id = $block->id;
            $hash = $this->coin->getBlockHash($block_id);
            $block->hash = $hash;
            $block->save();
            $i++;
        }
        $this->info("\n\r\n Finished! \n");

    }
}
