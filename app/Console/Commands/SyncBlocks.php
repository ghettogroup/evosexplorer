<?php

namespace App\Console\Commands;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\Transaction;
use Illuminate\Console\Command;


class SyncBlocks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync {coin?} {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       // return $this->updateBlocks();
        $coin = new Evos();

        try{
            $info = $coin->getInfo();
            if(isset($info['error'])){
                $this->info("RPC gave error! ". $info['error']);
                return;
            }
        }catch (\Exception $e){
            $this->info("Deamon is not working");
            return;
        }


        $maxBlock = $info['blocks'];
        $lastBlock = Block::orderBy('height','desc')->first(['height']);
        $syncedBlock = 1;
        $bar = $this->output->createProgressBar($maxBlock);
        if($lastBlock){
            $syncedBlock = $lastBlock['height']+1;
        }
        while ($syncedBlock <= $maxBlock){
            $bar->setProgress($syncedBlock);
            $coin->syncBlock($syncedBlock);
            $syncedBlock++;
        }
       $this->info('Finished');

    }

    public function updateBlocks(){
        $transactions = Transaction::with('raw')->skip(99999)->limit(1000000000)->get();

        $count = $transactions->count();
        $bar = $this->output->createProgressBar($count);
        $i =1;
        foreach ($transactions as $transaction){
            $bar->setProgress($i);

            $block_id = $transaction->raw->block_id;

            $transaction->block_id = $block_id;
            $transaction->save();
            $i++;
        }
        $this->info("\n\r\n Finished! \n");

    }
}
