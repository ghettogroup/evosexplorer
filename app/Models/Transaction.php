<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{

    protected $table = 'transactions';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'transaction_id',
        'block_id',
        'txid',
        'wallet_id',
        'way',
        'address',
        'value',
        'referer_txid',
        'n',
    ];

    public function raw()
    {
        return $this->belongsTo(RawTransaction::class,'transaction_id');
    }

    public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function block()
    {
        return $this->belongsTo('App\Models\Block');
    }


}