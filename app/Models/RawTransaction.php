<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RawTransaction extends Model
{

    protected $table = 'raw_transactions';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'block_id',
        'hex',
        'txid',
        'version',
        'locktime',
        'vin',
        'vout',
        'blockhash',
        'time',
        'blocktime',
    ];

    protected $casts = [
        'vin'=>'array',
        'vout'=>'array'
    ];

    public function block()
    {
        return $this->belongsTo('App\Models\Block');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction','transaction_id');
    }

}