<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Block extends Model
{

    protected $table = 'blocks';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'hash',
        'size',
        'height',
        'version',
        'merkleroot',
        'tx',
        'time',
        'nonce',
        'bits',
        'difficulty',
        'chainwork',
    ];
    protected $casts = [
        'tx' => 'array',
    ];

    public function raws()
    {
        return $this->hasMany('App\Models\RawTransaction');
    }

    public function stats()
    {
        return $this->hasMany('App\Models\Stat');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction','block_id','id');
    }

}