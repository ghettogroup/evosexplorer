<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{

    protected $table = 'wallets';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'address',
        'balance',
    ];

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

}