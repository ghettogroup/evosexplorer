<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stat extends Model
{

    protected $table = 'stats';
    public $timestamps = true;

    use SoftDeletes;

    protected $fillable = [
        'id',
        'block_id',
        'network_hash',
        'difficulty',
        'masternodes',
        'price',
    ];

    protected $dates = ['deleted_at'];

}