<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceDate extends Model
{

    protected $table = 'price_date';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'price',
        'date'
    ];


}
