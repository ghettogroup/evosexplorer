<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\RawTransaction;
use App\Models\Transaction;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MasternodeController extends Controller
{
    public function index()
    {
        $masternodes = $this->coin->getMasternodes();

        return $this->view('layouts.masternode',compact('masternodes'));

    }

}
