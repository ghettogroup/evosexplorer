<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $info;
    public $coin;

    public function __construct()
    {

        $this->coin = new Evos();
        $this->info = $this->coin->getInfo();
    }

    public function view($viewPath, Array $pageVars = [])
    {

        $ext['evos'] = collect();

        $ext['evos']->info = $this->info;
        $ext['evos']->coin = $this->coin;
        $ext['evos']->info['supply'] = $this->coin->getTotalSupply();

        foreach ($pageVars as $k => $v) {
            $ext[$k] = $v;
        }

        return view($viewPath, $ext);
    }

}
