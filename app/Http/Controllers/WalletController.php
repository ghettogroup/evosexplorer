<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\Transaction;
use App\Models\Wallet;
use Carbon\Carbon;

class WalletController extends Controller
{

    public function index()
    {
        $wallets = Wallet::orderBy('balance','desc')->limit(100)->get();

        return $this->view('layouts.top100',compact('wallets'));
    }

    public function show($id)
    {

        $wallet = Wallet::where('address', $id)->firstOrFail();

        $out = Transaction::where(['wallet_id' => $wallet->id, 'way' => 'out',])->sum('value');
        $in = Transaction::where(['wallet_id' => $wallet->id, 'way' => 'in',])->sum('value');


        $summary = [
            'out' => divide($out),
            'in' => divide($in),
            'balance' => divide($out - $in),
        ];

        $transactionsArray = Transaction::where('wallet_id', $wallet->id)->orderBy('block_id', 'desc')->paginate(25);
        $transactions = [];

        foreach ($transactionsArray as $transaction) {
            if (!isset($transactions[$transaction->txid])) {
                $transactions[$transaction->txid] = [
                    'value' => 0,
                    'way' => '',
                    'time' => $transaction->block->time
                ];
            }
            $transactions[$transaction->txid]['value'] += $transaction['value'];
            $transactions[$transaction->txid]['way'] = $transaction['way'];
        }

        return $this->view('layouts.wallet-info', compact('wallet', 'summary', 'transactions', 'transactionsArray'));
    }

    //========== API ==========//

    public function lastBlocks()
    {
        $lastBlocks = Block::with(['transactions' => function ($q) {
            return $q->where('way', 'out');
        }])->orderBy('id', 'desc')->limit('15')->get();

        $blocks = [];

        foreach ($lastBlocks as $lastBlock) {

            $block = [
                'height' => $lastBlock->height,
                'hash' => $lastBlock->hash,
                'value' => 0,
                'age' => Carbon::createFromTimestamp($lastBlock->time)->diffForHumans(),
                'recipients' => 0,
                'created' => date('Y-m-d H:i:s', $lastBlock->time) . ' UTC'
            ];

            $block['value'] = formatMoney(divide($lastBlock->transactions->sum('value')), 1);
            $block['recipients'] = $lastBlock->transactions->count();
            $blocks[] = $block;

        }

        return response()->json($blocks);
    }
}
