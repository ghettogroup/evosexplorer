<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\RawTransaction;
use App\Models\Transaction;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TxController extends Controller
{


    public function index()
    {
        $raws = RawTransaction::with(['transactions' => function ($q) {
            return $q->where('way', 'out');
        }])->with('block')->orderBy('block_id','desc')->orderBy('id')->paginate(15);

        $transactions = [];
        foreach ($raws as $raw) {
            $transactions[] = [
                'block_height' => $raw->block_id,
                'block_hash' => $raw->block->hash,
                'txid' => $raw->txid,
                'value'=> formatMoney(divide($raw->transactions->sum('value'))),
                'recipients' => $raw->transactions->count(),
                'age' => Carbon::createFromTimestamp($raw->block->time)->diffForHumans(),
                'created' => date('Y-m-d H:i:s', $raw->block->time) . ' UTC'
            ];
        }


        return $this->view('layouts.movement',compact('raws','transactions'));

    }


    public function show($id, Request $request)
    {

        $raw = RawTransaction::with('transactions', 'block')->where('txid', $id)->first();

        if(!$raw){
            $raw = collect($this->coin->getRawTransaction($id));

            if(isset($raw['error'])){
                abort(404);
            }
            $type='notconfirmed';

            $transaction = getTransactionDetailsRPC($raw);
        }else{
            $type='confirmed';
            $transaction = getTransactionDetails($raw);
        }


        $size = strlen($raw['hex'])/2;
        $info = [
            'size'=>$size,
            'time'=>isset($raw['time']) ?  date('Y-m-d H:i:s',$raw['time']) :'-',
            'block_hash'=>isset($raw['blockhash']) ? $raw['blockhash']: '',
            'block_height'=>isset($raw['block_id']) ? $raw['block_id'] : false,
            'confirmations'=>isset($raw['block_id']) ? $this->coin->getInfo()['blocks']-$raw['block_id'] : 'Unconfirmed',
            'out'=>$transaction['outTotal'],
            'in'=>$transaction['inTotal'],
            'fees'=>$transaction['fee'],
            'feeperkb'=>$transaction['fee']/$size*1000
        ];


        $wallet = $request->address;


        return $this->view('layouts.tx', compact('raw', 'transaction', 'block', 'wallet','info'));
    }

    //========== API ==========//

    public function lastTransactions()
    {
        $mempools = $this->coin->getRawMempool();

        $blocks = [];
        foreach ($mempools as $mempool){

            $raw = $this->coin->getRawTransaction($mempool);

            $block = [
                'height' => false,
                'hash' => false,
                'txid' => $raw['txid'],
                'value' => 0,
                'age' => '-',
                'recipients' => 0,
                'created' => '-'
            ];
            $value = 0;


            foreach ($raw['vout'] as $vout){
                $value += multiple($vout['value']);
            }
            $block['value'] = formatMoney(divide($value),1);
            $block['recipients'] = count($raw['vout'] );
            $blocks[] = $block;

        }



        $lastTransactions = RawTransaction::with(['transactions' => function ($q) {
            return $q->where('way', 'out');
        }])->orderBy('id', 'desc')->limit(15-count($mempools))->get();



        foreach ($lastTransactions as $lastTransaction) {

            $block = [
                'height' => $lastTransaction->block_id,
                'hash' => $lastTransaction->blockhash,
                'txid' => $lastTransaction->txid,
                'value' => 0,
                'age' => Carbon::createFromTimestamp($lastTransaction->block->time)->diffForHumans(),
                'recipients' => 0,
                'created' => date('Y-m-d H:i:s',$lastTransaction->block->time) . ' UTC'
            ];

            $block['value'] = formatMoney(divide($lastTransaction->transactions->sum('value')),1);
            $block['recipients'] = $lastTransaction->transactions->count();
            $blocks[] = $block;

        }

        return response()->json($blocks);
    }

}
