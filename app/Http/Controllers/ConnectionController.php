<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\RawTransaction;
use App\Models\Transaction;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ConnectionController extends Controller
{


    public function index()
    {

        $connectionCollection = $this->coin->getConnections();

        $connections = [];
        foreach ($connectionCollection as $connection){
            $ip = @explode(':',$connection['addr'])[0];
            $data = Cache::rememberForever($ip.'-cache',function() use($ip){
                return  json_decode(file_get_contents('http://ip-api.com/json/' . $ip), true);
            });
            $exp = explode('.',$ip);
            unset($exp[3]);
            $connections[] = [
                'ip' => @implode('.',$exp).'.XXX',
                'country'=> strtolower($data['countryCode']),
                'name'=>$data['country'],
                'version'=>$connection['subver'],
                'protocol'=> $connection['version']
            ];

        }

        return $this->view('layouts.connections',compact('connections'));

    }
    
}
