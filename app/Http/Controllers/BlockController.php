<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use Carbon\Carbon;

class BlockController extends Controller
{


    public function index(){
        $lastBlocks = Block::with(['transactions' => function ($q) {
            return $q->where('way', 'out');
        }])->orderBy('id', 'desc')->limit('15')->paginate(15);

        $blocks = [];

        foreach ($lastBlocks as $lastBlock) {

            $block = [
                'height' => $lastBlock->height,
                'hash' => $lastBlock->hash,
                'value' => 0,
                'age' => Carbon::createFromTimestamp($lastBlock->time)->diffForHumans(),
                'recipients' => 0,
                'created' => date('Y-m-d H:i:s', $lastBlock->time) . ' UTC'
            ];

            $block['value'] = formatMoney(divide($lastBlock->transactions->sum('value')),1);
            $block['recipients'] = $lastBlock->transactions->count();
            $blocks[] = $block;

        }
        return $this->view('blocks.index',compact('blocks','lastBlocks'));
    }

    public function show($id)
    {

        $block = Block::with('transactions', 'raws.transactions')->where('hash', $id)->firstOrFail();

        $blockValues = blockValues($block);

        $summary = [
            'numberOfTrans' => $block->raws->count(),
            'outputTotal' => $blockValues['out'],
            'transFee' => $blockValues['fee'],
            'height' => $block->height,
            'timestamp' => date("Y-m-d H:i:s", $block->time),
            'difficulty' => $block->difficulty,
            'bits' => hexdec($block->bits),
            'size' => $block->size,
            'version' => '0x' . $block->version . '0000000',
            'nonce' => $block->nonce,
            'type' => blockType($block),
            'confirmations' => $this->info['blocks'] - $block->height,
            'blockReward' => divide(blockReward($block->height)) . ' EVOS'
        ];

        $prev = Block::where('height', $block->height - 1)->first(['hash']);
        $next = Block::where('height', $block->height + 1)->first(['hash']);
        if($block->height == 0){
            $prev = ['hash'=>'THIS IS GENESIS BLOCK (no prev)'];
        }
        $hashes = [
            'hash' => $block->hash,
            'previous' => $prev ? $prev['hash'] : null,
            'next' => $next ? $next['hash'] : null,
            'chainwork' => $block->chainwork,
            'merkleroot' => $block->merkleroot
        ];
        $transactions = [];

        foreach ($block->raws as $raw) {
            $transactions[$raw->txid] = getTransactionDetails($raw);
        }


        return $this->view('layouts.block-detail', compact('transactions','hashes','summary','block'));
    }

    //========== API ==========//

    public function lastBlocks()
    {
        $lastBlocks = Block::with(['transactions' => function ($q) {
            return $q->where('way', 'out');
        }])->orderBy('id', 'desc')->limit('15')->get();

        $blocks = [];

        foreach ($lastBlocks as $lastBlock) {

            $block = [
                'height' => $lastBlock->height,
                'hash' => $lastBlock->hash,
                'value' => 0,
                'age' => Carbon::createFromTimestamp($lastBlock->time)->diffForHumans(),
                'recipients' => 0,
                'created' => date('Y-m-d H:i:s', $lastBlock->time) . ' UTC'
            ];

            $block['value'] = formatMoney(divide($lastBlock->transactions->sum('value')),1);
            $block['recipients'] = $lastBlock->transactions->count();
            $blocks[] = $block;

        }

        return response()->json($blocks);
    }
}
