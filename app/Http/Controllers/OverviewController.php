<?php

namespace App\Http\Controllers;

use App\Foundation\Evos;
use App\Models\Block;
use App\Models\PriceDate;
use App\Models\RawTransaction;
use App\Models\Stat;
use App\Models\Wallet;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OverviewController extends Controller
{


    public function index()
    {
        PriceDate::updateOrCreate([
            'date' => Carbon::now()->format('d M Y')
        ],[
            'price' => getCoinInfo()['daily_dollar']
        ]);

        return $this->view('layouts.overview');

    }

    public function stats()
    {
        $block = Block::orderBy('time', 'desc')->first();

        $blocks = [];
        $time = $label = $block->time;
        $dayago = Carbon::createFromTimestamp($block->time)->subDay()->getTimestamp();
        $labels = [];
        $label =Carbon::createFromTimestamp($label)->addDay()->getTimestamp();
        for ($i = 6; $i >= 0; $i--) {
            $label =Carbon::createFromTimestamp($label)->subDay()->getTimestamp();
            $labels[] = Carbon::createFromTimestamp($label)->format("d M");
        }
        $labels = array_values(array_reverse($labels));

        for ($i = 6; $i >= 0; $i--) {

            $blocksList = Block::whereBetween('time', [$dayago, $time])->get(['id'])->pluck('id')->toArray();
            $blocks[$i] = [min($blocksList), max($blocksList)];

            $time = Carbon::createFromTimestamp($time)->subDay()->getTimestamp();
            $dayago = Carbon::createFromTimestamp($dayago)->subDay()->getTimestamp();
        }
        $transactions = [];

        foreach ($blocks as $key => $block) {
            $transactions[$key] = RawTransaction::whereBetween('block_id', $block)->count();
        }
        $transactions = array_values(array_reverse($transactions));
        $networkhashes = [];

        foreach ($blocks as $key => $block) {
            $networkhashes[$key] = Stat::whereBetween('block_id', $block)->avg('network_hash')/1000000000;
        }
        $networkhashes = array_reverse($networkhashes);

        $masternodes = [];

        foreach ($blocks as $key => $block) {
            $masternodes[$key] = Stat::whereBetween('block_id', $block)->avg('masternodes');
        }
        $masternodes = array_values(array_reverse($masternodes));

        return $this->view('stats', compact('masternodes', 'networkhashes', 'transactions','labels'));
    }

//    public function mainStats()
//    {
//        $block = Block::orderBy('time', 'desc')->first();
//
//        $block_count = Block::count();
//
//        $blocks = [];
//        $time = $label = $block->time;
//        $dayago = Carbon::createFromTimestamp($block->time)->subDay()->getTimestamp();
//        $labels = [];
//        $label =Carbon::createFromTimestamp($label)->addDay()->getTimestamp();
//        for ($i = 6; $i >= 0; $i--) {
//            $label =Carbon::createFromTimestamp($label)->subDay()->getTimestamp();
//            $labels[] = Carbon::createFromTimestamp($label)->format("d M");
//        }
//        $labels = array_values(array_reverse($labels));
//
//        for ($i = 6; $i >= 0; $i--) {
//
//            $blocksList = Block::whereBetween('time', [$dayago, $time])->get(['id'])->pluck('id')->toArray();
//            $blocks[$i] = [min($blocksList), max($blocksList)];
//
//            $time = Carbon::createFromTimestamp($time)->subDay()->getTimestamp();
//            $dayago = Carbon::createFromTimestamp($dayago)->subDay()->getTimestamp();
//        }
//        $transactions = [];
//
//        foreach ($blocks as $key => $block) {
//            $transactions[$key] = RawTransaction::whereBetween('block_id', $block)->count();
//        }
//        $transactions = array_values(array_reverse($transactions));
//        $networkhashes = [];
//
//        foreach ($blocks as $key => $block) {
//            $networkhashes[$key] = Stat::whereBetween('block_id', $block)->avg('network_hash')/1000000000;
//        }
//        $networkhashes = array_reverse($networkhashes);
//
//        $masternodes = [];
//
//        foreach ($blocks as $key => $block) {
//            $masternodes[$key] = Stat::whereBetween('block_id', $block)->avg('masternodes');
//        }
//        $masternodes = array_values(array_reverse($masternodes));
//
//        $return = [
//            "masternodes"   => $masternodes,
//            "networkhash"   => $networkhashes,
//            "total_blocks"  => $block_count
//        ];
//
//        return response()->json($return);
//    }

    public function search(Request $request)
    {
        $q = $request->q;
        if (is_numeric($q)) {
            $block = Block::findOrFail($q);
            if ($block && $block->hash) {
                Session::push('search', ['block',$block->hash]);
                return redirect(route('block.show', $block->hash));
            }
        }
        $block = Block::where('hash', $q)->first();
        if ($block && $block->hash) {
            Session::push('search', ['block',$block->hash]);
            return redirect(route('block.show', $block->hash));
        }

        $transaction = RawTransaction::where('txid', $q)->first();

        if ($transaction) {
            Session::push('search', ['tx',$transaction->txid]);
            return redirect(route('tx.show', $transaction->txid));
        }

        $wallet = Wallet::where('address', $q)->first();

        if ($wallet && $wallet->address) {
            Session::push('search', ['address',$wallet->address]);
            return redirect(route('address.show', $wallet->address));
        }

//        $transaction = $this->coin->getRawTransaction($q);
//        if (!isset($transaction['error'])) {
//            Session::push('search', ['tx',$q]);
//            return redirect(route('tx.show', $q));
//        }

        abort(404);
    }

    public function getinfo(){
        return response()->json($this->coin->getInfo());
    }
    public function getnetworkhashps(){
        return response($this->coin->getNetworkHash());
    }
    public function getMasterNodeCount(){
        return response()->json($this->coin->getMasternodeCount());
    }
    public function supply(){

        return supply($this->info['blocks']);
    } public function totalWallets(){

        return Wallet::count();
    }

    public function searchDelete(Request $request)
    {
        $search = Session::get('search');

        $type = $request->get('type');
        $hash = $request->get('hash');
        $new = [];

        Session::forget('search');
        $id = "";

        foreach($search as $s) {
            if(in_array($type, $s) && in_array($hash, $s)) {
                $id = $type . "_" . $hash;
                continue;
            }
            $new[] = $s;
        }

        Session::put('search', $new);

        return $id;
    }
}

