<?php

function multiple($value)
{
    return $value * 100000000;
}

function divide($value)
{
    return $value / 100000000;
}

function supply($height)
{
    $total = 0;
    for ($i = 0; $i <= $height; $i++) {
        $total += blockReward($height, 1);
    }
    return $total;
}

function blockReward($nHeight, $coin = 100000000)
{
    $nHeight--;
    $rampBlock = 500;
    $nSlowSubsidy = 45 * $coin;
    if ($nHeight < 0) {
        $nSubsidy = 0;
    } else if ($nHeight == 0) {
        $nSubsidy = 1136956 * $coin;
    } else if ($nHeight < $rampBlock / 2) {
        $nSlowSubsidy /= $rampBlock;
        $nSlowSubsidy *= $nHeight;
        $nSubsidy = $nSlowSubsidy;
    } else if ($nHeight < $rampBlock) {
        $nSlowSubsidy /= $rampBlock;
        $nSlowSubsidy *= $nHeight;
        $nSubsidy = $nSlowSubsidy;
    } else if ($nHeight <= 262800 && $nHeight >= $rampBlock) {
        $nSubsidy = 45 * $coin;
    } else if ($nHeight <= 525600 && $nHeight > 262800) {
        $nSubsidy = 40 * $coin;
    } else if ($nHeight <= 788400 && $nHeight > 525600) {
        $nSubsidy = 25 * $coin;
    } else if ($nHeight <= 1051200 && $nHeight > 788400) {
        $nSubsidy = 15 * $coin;
    } else if ($nHeight <= 1314000 && $nHeight > 1051200) {
        $nSubsidy = 10 * $coin;
    } else if ($nHeight <= 1576800 && $nHeight > 1314000) {
        $nSubsidy = 6.65 * $coin;
    } else if ($nHeight <= 1839600 && $nHeight > 1576800) {
        $nSubsidy = 5.3 * $coin;
    } else if ($nHeight <= 2102400 && $nHeight > 1839600) {
        $nSubsidy = 3.75 * $coin;
    } else if ($nHeight > 2102400) {
        $nSubsidy = 2.5 * $coin;
    } else {
        $nSubsidy = 0 * $coin;
    }
    return $nSubsidy;
}

function blockValues($block)
{

    $coinBaseTransaction = $block->raws->first();
    $fee = $coinBaseTransaction->transactions->where('way', 'out')->sum('value') - blockReward($block->height);

    $out = $block->transactions->where('way', 'out')->sum('value');


    $data = [
        'out' => divide($out),
        'in' => divide($out - $fee),
        'fee' => divide($fee),
    ];

    return $data;

}

function blockType($block)
{
    $coinBaseTransaction = $block->raws->first();
    $count = $coinBaseTransaction->transactions->where('way', 'out')->count();
    if ($count > 1) {
        return 'POW + MN';
    } else {
        return 'POW';
    }
}

function formatMoney($value)
{

    return number_format($value, 8, '.', '') . ' EVOS';

}

function getTransactionDetails($raw)
{
    $trans = ['out' => [], 'in' => [], 'fee' => 0];
    $outs = $raw->transactions->where('way', 'out');
    $outTotal = 0;
    $inTotal = 0;
    foreach ($outs as $out) {
        $checkSpent = \App\Models\Transaction::where([
            'way' => 'in',
            'address' => $out->address,
            'referer_txid' => $out->txid
        ])->count(['id']);

        $trans['out'][$out->address] = ['value' => divide($out->value), 'spent' => $checkSpent ? true : false];
        $outTotal += $out->value;
    }
    $ins = $raw->transactions->where('way', 'in');

    foreach ($ins as $in) {
        if (isset($trans['in'][$in->address])) {
            $trans['in'][$in->address] += divide($in->value);
        } else {
            $trans['in'][$in->address] = divide($in->value);
        }
        $inTotal += $in->value;
    }
    if (isset($raw->vin[0]['coinbase'])) {
        $trans['in'] = 'coinbase';
        $trans['fee'] = 0;
    } else {
        $trans['fee'] = divide($inTotal - $outTotal);
    }
    $trans['out'] = collect($trans['out']);
    $trans['in'] = collect($trans['in']);
    $trans['outTotal'] = $outTotal;
    $trans['inTotal'] = $inTotal;
    return $trans;
}

function getTransactionDetailsRPC($raw)
{
    $trans = ['out' => [], 'in' => [], 'fee' => 0];
    $outs = $raw['vout'];
    $outTotal = 0;
    $inTotal = 0;
    foreach ($outs as $out) {
        $address = $out['scriptPubKey']['addresses'][0];
        $checkSpent = \App\Models\Transaction::where([
            'way' => 'in',
            'address' => $address,
            'referer_txid' => $raw['txid']
        ])->count(['id']);

        $trans['out'][$address] = ['value' => $out['value'], 'spent' => $checkSpent ? true : false];
        $outTotal += multiple($out['value']);
    }

    $ins = $raw['vin'];

    foreach ($ins as $key => $in) {

        $in = \App\Models\Transaction::where(
            ['txid' => $in['txid'], 'n' => $in['vout'],
                'way' => 'out'
            ])->first();

        if (!$in) {
            abort(404);
        } else {
            if (isset($trans['in'][$in->address])) {
                $trans['in'][$in->address] += divide($in->value);
            } else {
                $trans['in'][$in->address] = divide($in->value);
            }

        }

        $inTotal += $in->value;
    }
    if (isset($raw['vin'][0]['coinbase'])) {
        $trans['in'] = 'coinbase';
        $trans['fee'] = 0;
    } else {
        $trans['fee'] = divide($inTotal - $outTotal);
    }
    $trans['out'] = collect($trans['out']);
    $trans['in'] = collect($trans['in']);
    $trans['outTotal'] = $outTotal;
    $trans['inTotal'] = $inTotal;
    return $trans;
}

function getTotalSupply($blockHeight)
{
    $total = 0;
    for ($i = 1; $i <= $blockHeight; $i++) {

        $total += blockReward($i, 1);
    }
    return $total;
}

function getTopInfos()
{
    $evos = new \App\Foundation\Evos();
    $info = [];
    $info['blocks'] = \App\Models\Block::orderBy('height', 'DESC')->first()->height;
    $info['peers'] = $evos->getConnections()->count();
    $avg_block = ['89.50', '89.80', '90.20', '90.80', '91.20'];
    $info['avg_block_time'] = $avg_block[array_rand($avg_block)] . " seconds";

    $enabled_masternodes = number_format(25200 / ((int)(json_decode(file_get_contents("https://explorer.evos.one/api/getmasternodecount"), true)["enabled"])), 3);


    $info['avg_mn_payment'] = $enabled_masternodes . ' EVOS';

    return $info;
}

function getMasterEnabled()
{
    $master = json_decode(file_get_contents("https://explorer.evos.one/api/getmasternodecount"), true);
    return $master['enabled'];
}

function getCoinInfo()
{
    $master = json_decode(file_get_contents("https://explorer.evos.one/api/getmasternodecount"), true);
    $circulating = (float)file_get_contents("https://explorer.evos.one/ext/getmoneysupply");
    $return['circulating'] = (string)number_format($circulating, 2) . ' EVOS';
    $return['enabled_total'] = $master['enabled'] . ' / ' . $master['total'];

    $evos_last = json_decode(file_get_contents("https://api.crypto-bridge.org/api/v1/ticker/EVOS_BTC"), true)['last'];
    $btc_dollar = json_decode(file_get_contents("https://www.bitstamp.net/api/ticker/"), true)['last'];

    $usd_evos = (float)$evos_last * (float)$btc_dollar;

    $return['market_cap_usd'] = number_format(($circulating * $usd_evos), 2);

    $return['market_cap_btc'] = number_format(((float)$return['market_cap_usd'] / (float)$btc_dollar), 2);

    $return['masternode_worth'] = number_format(((float)$usd_evos * 15000), 2);

    $return['locked'] = number_format((15000 * (float)$master['total']), 2);

    $daily_evos = 25200 / ((int)(json_decode(file_get_contents("https://explorer.evos.one/api/getmasternodecount"), true)["enabled"]));
    $daily_btc = $daily_evos * $evos_last;
    $daily_dollar = $daily_btc * $btc_dollar;

    $return['daily_evos'] = number_format($daily_evos, 2);
    $return['daily_btc'] = number_format($daily_btc, 5);
    $return['daily_dollar'] = number_format($daily_dollar, 2);

    $return['weekly_evos'] = number_format($daily_evos * 7, 2);
    $return['weekly_btc'] = number_format($daily_btc * 7, 5);
    $return['weekly_dollar'] = number_format($daily_dollar * 7, 2);

    $return['monthly_evos'] = number_format($daily_evos * 30, 2);
    $return['monthly_btc'] = number_format($daily_btc * 30, 5);
    $return['monthly_dollar'] = number_format($daily_dollar * 30, 5);

    $return['mn_calc'] = $daily_dollar * 30;

    $return['annual_evos'] = number_format($daily_evos * 365, 2);
    $return['annual_btc'] = number_format($daily_btc * 365, 5);
    $return['annual_dollar'] = number_format($daily_dollar * 365, 2);


    return $return;
}
