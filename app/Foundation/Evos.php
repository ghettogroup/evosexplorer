<?php


namespace App\Foundation;

use App\Models\Block;
use App\Models\RawTransaction;
use App\Models\Stat;
use App\Models\Transaction;
use App\Models\Wallet;

class Evos
{


    protected $client;
    protected $config;

    public function __construct()
    {


        $this->client = new RPCClient(
            env('RPCHOST'),
            env('RPCPORT'),
            env('RPCUSER'),
            env('RPCPASS')
        );
    }

    public function multiple($value)
    {
        return $value * 100000000;
    }

    public function getInfo()
    {
        $data = [];
        $getinfo = $this->client->call('getinfo', []);

        return $getinfo;
    }

    public function getBlockHash(int $blockNumber)
    {
        return $this->client->call('getblockhash', [$blockNumber]);
    }

    public function getBlock($blockHash)
    {
        return $this->client->call('getblock', [$blockHash]);
    }

    public function getBlockById(int $blockNumber)
    {
        $hash = $this->getBlockHash($blockNumber);
        return $this->getBlock($hash);
    }

    public function getRawTransaction($transactionHash)
    {
        $transaction = $this->client->call('getrawtransaction', [$transactionHash, 1]);
        return $transaction;
    }

    private function inOut($transaction)
    {
        $data = [];
        if (isset($transaction->vin)) {
            $this->vIn($transaction->vin, $transaction);
        }
        if (isset($transaction->vout)) {
            $this->vOut($transaction->vout, $transaction);
        }

    }

    private function vIn($inArray, $transaction)
    {
        $i = 0;
        $array = [];
        foreach ($inArray as $in) {
            if (!isset($in['coinbase'])) {
                $data = Transaction::where('txid', $in['txid'])
                    ->where('n', $in['vout'])
                    ->where('way','out')->first();
                if($data){
                    $data =  $data->toArray();
                    $data['referer_txid'] = $data['txid'];
                    $data['txid'] = $transaction->txid;
                    $data['way'] = 'in';
                    $data['n'] = $i;
                    $data['transaction_id'] = $transaction->id;
                    if($data['wallet_id']){
                        $data['wallet_id'] = $this->checkAddress($data['address'], $data['value'] * -1);
                    }else{
		        $data['wallet_id'] = null;
		    }
                    unset($data['id'],$data['created_at'],$data['updated_at']);
                    $data['block_id'] = $transaction->block_id;
                    $array[] = $data;
                    $i++;
                }

            }
        }
        Transaction::insert($array);

    }

    private function vOut($outArray, $transaction)
    {
        $array = [];
        foreach ($outArray as $out) {
            $data = [];
            $data['value'] = $this->multiple($out['value']);

            if (isset($out['scriptPubKey']['addresses'])) {
                if (count($out['scriptPubKey']['addresses']) > 1) {
                    dump('Multiple Address');
                    dd($out);
                }
                $data['address'] = $out['scriptPubKey']['addresses'][0];
                $data['wallet_id'] = $this->checkAddress($data['address'], $data['value']);
            }else{
 		$data['address'] = null;
		$data['wallet_id'] = null;
	    }
            $data['n'] = $out['n'];
            $data['way'] = 'out';
            $data['txid'] = $transaction->txid;
            $data['transaction_id'] = $transaction->id;
            $data['block_id'] = $transaction->block_id;
            $array[] = $data;
        }
        Transaction::insert($array);
    }

    public function syncBlock($syncedBlock)
    {
        $blockHash = $this->getBlockHash($syncedBlock);
        $blockData = $this->getBlock($blockHash);
        $blockData['hash'] = $blockHash;
        $blockData['id'] = $blockData['height'];

        $block = Block::create($blockData);
        $block->created_at = date("Y-m-d H:i:s",$blockData['time']);
        $block->save();
        $this->syncTx($block->tx, $block->id);
        $this->syncStats($block);
    }

    public function syncStats($block){

        try{
            $price = json_decode(file_get_contents('https://www.coinexchange.io/api/v1/getmarketsummary?market_id=741'), 1)['result']['LastPrice'];
        }catch (\Exception $e){
            $price = 0;
        }

        $data = [
            'difficulty'=>$block->difficulty,
            'network_hash'=>$this->getNetworkHash($block->height),
            'masternodes'=>$this->getMasternodeCount('total'),
            'price'=>$price,
            'block_id'=> $block->id
        ];

        Stat::create($data);

    }

    private function syncTx($txs, $blockId)
    {
        foreach ($txs as $tx) {
            $transactionData = $this->getRawTransaction($tx);
            $transactionData['block_id'] = $blockId;
            $transaction = RawTransaction::create($transactionData);
            $this->inOut($transaction);
        }
    }

    private function checkAddress($address, $value)
    {
        $wallet = Wallet::where('address', $address)->first();
        if ($wallet) {
            $wallet->balance = $wallet->balance + $value;
            $wallet->save();
            return $wallet->id;
        } else {

            $wallet = Wallet::create([
                'address' => $address,
                'balance' => $value
            ]);
            return $wallet->id;
        }

    }

    public function getNetworkHash($blockId = null)
    {
        if($blockId){
            return $this->client->call('getnetworkhashps', [$blockId]);
        }
        return $this->client->call('getnetworkhashps', []);

    }

    public function getMasternodeCount($param = null){
        $count =  $this->client->call('getmasternodecount', []);
        if($param && isset($count[$param])){
            return $count[$param];
        }
        return $count;
    }

    public function getTotalSupply()
    {
        $info = $this->getInfo();
        return getTotalSupply($info['blocks']);
    }

    public function getMasternodes()
    {
        return collect($this->client->call('masternode', ['list']))->sortByDesc('lastpaid')->sortBy('activetime');
    }

    public function getConnections()
    {
        return collect($this->client->call('getpeerinfo', []));
    }

    public function getRawMempool()
    {
        return collect($this->client->call('getrawmempool', []));
    }


}
