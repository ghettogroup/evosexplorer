<?php
/**
 * Created by PhpStorm.
 * User: ulash
 * Date: 4.01.2018
 * Time: 19:45
 */

namespace App\Foundation;

use JsonRPC\Client;
use Exception;


class RPCClient
{

    protected $rpc_host;
    protected $rpc_port;
    protected $rpc_user;
    protected $rpc_password;
    protected $client;

    public function __construct($rpc_host, $rpc_port, $rpc_user, $rpc_password)
    {
        $this->rpc_host = $rpc_host;
        $this->rpc_port = $rpc_port;
        $this->rpc_user = $rpc_user;
        $this->rpc_password = $rpc_password;
        $this->client = new Client($this->rpc_host . ':' . $rpc_port);
        $this->client->authentication($this->rpc_user, $this->rpc_password);
  }

    public function call($method, $param)
    {
        try {
            $response = $this->client->execute($method, $param);
        } catch (Exception $e) {
dd($e);
            $response = ['error' => $e->getMessage()];
        }
        return $response;
    }


}
