$(function () {
    window.url = location.protocol + '//' + location.host + '/';
    // mainStats();
    checkLastTransactions();
    checkLastBlocks();

});

// function mainStats() {
//
// }

function checkLastBlocks() {
    if ($('#lastBlocks').length) {
        var lastBlocks = get('lastBlocks', function (response) {
            $('#lastBlocks tbody').html('');

            for (var i in response) {
                if(i == 0){
                    $('.block-height').html(response[i].height)
                }
                $('#lastBlocks tbody').append(
                    '<tr> <td class="text-center"><a href="' + window.url + 'block/' + response[i].hash + '">' + response[i].height + '</a></td>' +
                    ' <td><div class="font-w600 font-size-base"> <a href="'+window.url+'block/'+response[i].hash+'">'+response[i].hash+'</a></div></td>' +
                    '<td class="font-size-base"> ' + response[i].value + '</td>' +
                    '<td class="font-size-base"><span class="badge badge-success">' + response[i].age +'</span> </td>' +
                    '<td class="font-size-base"> ' + response[i].recipients + '</td>' +
                    '<td class="d-none d-sm-block font-size-base">' + response[i].created + '</td> </tr>'
                )
            }
        });
        setTimeout(function(){checkLastBlocks()},10000);
    }
}

function checkLastTransactions() {
    if ($('#lastTransactions').length) {
        var lastBlocks = get('lastTransactions', function (response) {
            $('#lastTransactions tbody').html('');

            for (var i in response) {

                var part = '';
                if(response[i].height){
                    part +=  '<tr><th class="text-center" scope="row"><a href="' + window.url + 'block/' + response[i].hash + '">' + response[i].height + '</a></td>';

                }else{
                    part += '<tr> <td class="text-center scope="row">-</td>';
                }

                part += ' <td class="font-w600"><a href="'+window.url+'tx/'+response[i].txid+'">'+response[i].txid+'</a></div></td>' +
                    '<td class="font-w600">' + response[i].value +'</span> </td>' +
                    '<td class="font-w600">' + response[i].age + '</td>' +
                    '<td class="font-w600">' + response[i].recipients + '</td>' +
                    '<td class="font-w600">' + response[i].created + '</td> </tr>';

                $('#lastTransactions tbody').append(
                    part
                )
            }
        });
        setTimeout(function(){checkLastTransactions()},10000);
    }
}


function get(get, handleData) {

    $.get(location.protocol + '//' + location.host + '/api/' + get, function (data) {
        handleData(data);
    })

}
