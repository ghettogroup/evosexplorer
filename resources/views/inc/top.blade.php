<div class="row">
    <div class="col-md-12 col-lg-9">
        <div class="row">
            <div class="col-md-6">
                <a class="block block-rounded block-link-shadow f-height" href="javascript:void(0)">
                    <div class="block block-rounded block-bordered">
                        <div class="block-header block-header-white">
                            <h3 class="block-title">Status</h3>
                        </div>
                        <div class="block-content">
                            <div class="table-responsive">
                                <table class="table table-borderless table-vcenter">
                                    @php($info = getTopInfos())
                                    <tbody>
                                    <tr>
                                        <th class="font-w600" scope="row">Status:</th>
                                        <td class="font-w600 text-success">
                                            Online
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">Blocks:</th>
                                        <td class="font-w600 text-primary-dark">
                                            {!! $info['blocks'] !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">Peers:</th>
                                        <td class="font-w600">
                                            {!! $info['peers'] !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">Avg. Block Time:</th>
                                        <td class="font-w600">
                                            {!! $info['avg_block_time'] !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">Avg. MN Payment:</th>
                                        <td class="font-w600">
                                            {!! $info['avg_mn_payment'] !!}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a class="block block-rounded block-link-shadow f-height" href="javascript:void(0)">
                    <div class="block-header">
                        <h3 class="block-title">Masternode Calculator</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <form action="#">
                                <div class="form-group col-6 float-left">
                                    <input type="text" class="form-control" id="pos-input" name="pos-input"
                                           placeholder="0">
                                </div>
                                <div class="form-group col-6 float-left">
                                    <button type="button" class="btn btn-hero-primary calculate-mn">Estimate</button>
                                </div>
                            </form>
                            <div class="col-12 expl">
                                In order to make a right calculation and see monthly USD income put your masternode
                                count.
                            </div>
                        </div>
                        <div class="row text-center mt-4 col-10 offset-1 form-group mn_result">
                            <input readonly value="" class="form-control">
                        </div>
                    </div>
                </a>
            </div>
        </div>
        {{--{!! $coininfo['enabled_total'] !!}--}}
        @php($activeMastenode = getMasterEnabled())
        <div class="row">
            <div class="col-md-6">
                <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
                    <div class="block-header">
                        <h3 class="block-title">USD Price</h3>
                    </div>
                    <div class="block-content block-content-full text-center">
                        <div class="py-3 mt-4">
                            <script src="https://widgets.coingecko.com/coingecko-coin-ticker-widget.js"></script>
                            <coingecko-coin-ticker-widget coin-id="acreage-coin" currency="usd" height="300"
                                                          locale="en"></coingecko-coin-ticker-widget>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
                    <div class="block-header">
                        <h3 class="block-title">Masternode</h3>
                    </div>
                    <div class="block-content block-content-full text-center">
                        <div class="block-content block-content-full d-flex justify-content-between">
                            <div class="mr-3">
                                <p class="font-size-h3 font-w300 mb-0">
                                    {!! $activeMastenode !!}
                                </p>
                                <p class="text-muted mb-0">
                                    Masternodes
                                </p>
                            </div>
                            <div>
                                <i class="fa fa-2x fa-coins text-white-75"></i>
                            </div>
                        </div>

                        <div class="block-content block-content-full overflow-hidden">
                            <span class="js-sparkline" data-type="line"
                                  data-points="[{!! (int)$activeMastenode+5 !!},{!! (int)$activeMastenode-2 !!},{!! $activeMastenode+3 !!},{!! (int)$activeMastenode-2 !!},{!! (int)$activeMastenode !!}]"
                                  data-width="300px"
                                  data-height="100px"
                                  data-line-color="#e04f1a"
                                  data-fill-color="transparent"
                                  data-spot-color="transparent"
                                  data-min-spot-color="transparent"
                                  data-max-spot-color="transparent"
                                  data-highlight-spot-color="#e04f1a"
                                  data-highlight-line-color="#e04f1a"
                                  data-tooltip-suffix="Masternodes">
                            </span>
                        </div>


                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-3">
        <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
            <div class="block-header">
                <h3 class="block-title">Search History</h3>
            </div>
            @if(session('search'))
                <div>
                    @foreach(session('search') as $search)
                        @if($loop->iteration == 10) @break @endif
                        <p id="{!! implode('_', $search) !!}" class="mb-1"
                           style="border: 1px solid blue;border-radius: 15px;padding: 5px 20px;">
                            <a class="mt-1"
                               href="{!! route($search[0].'.show', $search[1]) !!}">{!! str_limit($search[1], 17) !!}</a>
                            <i class="close fa fa-times ml-2" data-type="{!! $search[0] !!}"
                               data-hash="{!! $search[1] !!}"></i>
                        </p>
                    @endforeach
                </div>
            @endif
        </a>
    </div>
</div>

@push('scripts')
    <script>
        $('p .block.block-rounded.block-link-shadow').remove();

        $('.calculate-mn').click(function () {
            let masternode_count = $('[name=pos-input]').val();
            let dollars = "{!! getCoinInfo()['mn_calc'] !!}";
            let price = masternode_count * dollars;

            $('.mn_result').find('input').show().val(price + ' $');
        });

        $('i.close').click(function () {
            let type = $(this).attr("data-type");
            let hash = $(this).attr("data-hash");
            $.ajax({
                url: "/search/delete",
                data: {type: type, hash: hash, _token: "{!! csrf_token() !!}"},
                type: "POST",
                success: function (data) {
                    if ($("#" + data).length > 0) {
                        $("#" + data).remove();
                    }
                }
            });
        });
    </script>
@endpush
