<nav id="sidebar" aria-label="Main Navigation">

    <div class="header-dark-gray" style="background-color:white;">
        <div class="content-header">

            <a class="link-fx font-w600 font-size-lg text-white" href="{!! url('/') !!}">
                <span class="smini-visible">
                    <img class="sidebar-logo" width="30" style="margin-left: 0" src="{{asset('images/logo2.png')}}"
                         alt="">
                </span>
                <span class="smini-hidden p-md-6">
                   <img class="sidebar-logo" width="50" src="{{asset('images/logo2.png')}}" alt="">
                </span>
                <span class="smini-hidden" style="color: #4b4c56;">
                    ACREAGE <em style="color: #da9042; opacity: .8;">Explorer</em>
                </span>
            </a>
            <div>
                <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close"
                   href="javascript:void(0)">
                    <i class="fa fa-times-circle"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="content-side content-side-full">
        <ul class="nav-main">
            <li class="nav-main-heading">MENU</li>
            <li class="nav-main-item">
                <a class="nav-main-link" href="{!! url('/') !!}">
                    <i class="nav-main-link-icon fa fa-home text-white"></i>
                    <span class="nav-main-link-name">Overview</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link " aria-expanded="false" href="{!! url('/movements') !!}">
                    <i class="nav-main-link-icon fa fa-angle-double-right text-white"></i>
                    <span class="nav-main-link-name">Movements</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link " aria-expanded="false" href="{!! url('/top100') !!}">
                    <i class="nav-main-link-icon fa fa-coins text-white"></i>
                    <span class="nav-main-link-name">Top 100</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link " aria-expanded="false" href="{!! url('/masternode') !!}">
                    <i class="nav-main-link-icon fa fa-cube text-white"></i>
                    <span class="nav-main-link-name">Masternode</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link " aria-expanded="false" href="{!! url('/connections') !!}">
                    <i class="nav-main-link-icon fa fa-compress text-white"></i>
                    <span class="nav-main-link-name">Connections</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link " aria-expanded="false" href="{!! url('/statics') !!}">
                    <i class="nav-main-link-icon fa fa-chart-line text-white"></i>
                    <span class="nav-main-link-name">Statistics</span>
                </a>
            </li>
            <li class="nav-main-item">
                <a class="nav-main-link " aria-expanded="false" href="{!! url('/coin-info') !!}">
                    <i class="nav-main-link-icon fa fa-info text-white"></i>
                    <span class="nav-main-link-name">Coin Info</span>
                </a>
            </li>
        </ul>
    </div>
</nav>

<header id="page-header" class="header-dark-gray">
    <div id="page-header-search" class="header-dark-gray">
        <div class="float-left mt-3 ml-3">
            <button type="button" class="btn btn-dual mr-1 btn-blk" data-toggle="layout" data-action="sidebar_toggle">
                <i class="fa fa-fw fa-bars"></i>
            </button>
        </div>
        <div class="content-header">
            <form class="w-100 search_form" action="{!! url('search') !!}" method="POST">
                {!! csrf_field() !!}
                <div class="input-group">
                    <input type="text" class="form-control border-0" placeholder="Search..."
                           id="page-header-search-input" name="q">
                </div>
            </form>
        </div>
    </div>
</header>

@push('scripts')
    <script>
        $(window).keypress(function (e) {
            if (e.which === 13) {
                let search = $("#page-header-search-input").val();
                if (search.length === 0) {
                    return false;
                } else {
                    $('.search_form').submit();
                }
            }
        });

    </script>
@endpush
