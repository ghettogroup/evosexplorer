<footer id="page-footer" class="mt-7 pt-5 mb-0">
    <div class="content">
        <div class="row font-size-sm">
            <div class="col-md-4" style="float:left">
                <div class="block" href="javascript:void(0)">
                    <div class="block-content block-content-full text-center mt-3">
                        <div class="col-3 float-left mr-0">
                            <img class="img-fluid" src="{!! asset('images/logo2.png') !!}">
                        </div>
                        <div class="col-9 float-left ml-0">
                            <p class="mb-0 mt-3">Copyright © 2018 ACREAGE</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block" href="javascript:void(0)">
                    <div class="block-content block-content-full text-center">
                        <div class="table-responsive">
                            <table class="table table-borderless table-vcenter">
                                <tbody>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">STATUS</th>
                                    <th class="font-w600" scope="row">BLOCKS</th>
                                    <th class="font-w600" scope="row">TIME</th>
                                </tr>
                                <tr class="mb-0">
                                    <td class="font-w600 text-success">
                                        ONLINE
                                    </td>
                                    <td class="font-w600">
                                        <strong>{!! getTopInfos()['blocks'] !!}</strong>
                                    </td>
                                    <td class="font-w600">
                                        {!! gmdate("H:i") !!} UTC
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block" href="javascript:void(0)">
                    <div class="block-content block-content-full ">
                        {{--<div class="row">SOCIAL</div>--}}
                        <div class="row mt-4">
                            <ul class="social text-body-bg-dark">
                                <li class="float-left mr-1 ml-1">
                                    <a href="https://www.instagram.com/acreagecoin" target="_blank"><i class="fas fa-instagram fa-2x fab footer__social-media-icon"></i> </a>
                                </li>
                                <li class="float-left mr-1 ml-1">
                                    <a href="https://discord.gg/34rEw4D" target="_blank"><i class="fas fa-discord fa-2x fab footer__social-media-icon"></i> </a>
                                </li>
                                <li class="float-left mr-1 ml-1">
                                    <a href="https://bitcointalk.org/index.php?topic=4709055" target="_blank"><i class="fas fa-bitcoin fa-2x fab footer__social-media-icon"></i> </a>
                                </li>
                                {{--<li class="float-left mr-1 ml-1">
                                    <a href="https://masternodes.online/currencies/EVOS/" target="_blank"><i class="fas fa-reddit fa-2x fab footer__social-media-icon"></i> </a>
                                </li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
