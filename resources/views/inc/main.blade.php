<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Explorer | ACREAGE</title>

    <meta name="description" content="ACREAGE Explorer">
    <meta name="author" content="GG Dev">
    <meta name="robots" content="index, nofollow">

    <meta property="og:title" content="ACREAGE Explorer">
    <meta property="og:site_name" content="ACREAGE Explorer">
    <meta property="og:description" content="ACREAGE Explorer">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <link rel="shortcut icon" href="{{ asset("images/fav-icon.ico") }}">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/dashmix.min.css?v=2') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/style.css?v=4') }}">
</head>
<body>
<div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed main-content-boxed side-trans-enabled">

    @include('inc.sidebar')

    @yield('content')

    @include('inc.footer')
</div>
<script src="{{ asset('assets/js/dashmix.core.min.js') }}"></script>
<script src="{{ asset('assets/js/dashmix.app.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/chart.js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/be_pages_dashboard.min.js') }}"></script>
<script>jQuery(function(){ Dashmix.helpers('sparkline'); });</script>
{{--<script src="{{ asset('assets/js/pages/be_comp_charts.min.js')}}"></script>--}}
<script src="{{ asset('assets/js/evos.js?v=5')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@stack('scripts')
</body>
</html>
