@extends('inc.main')

@section('content')
    <main id="main-container">
        <div class="content">
            @include('inc.top')

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">MASTERNODES</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-vcenter">
                                <thead>
                                <tr>
                                    <th>LAST PAID</th>
                                    <th>ACTIVE</th>
                                    <th>ADDRESS</th>
                                    <th>COLLATERAL TX</th>
                                    <th>INDEX</th>
                                    <th>VERSION</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($masternodes as $masternode)
                                    <tr>
                                        <td class="font-w600">{!! $masternode['lastpaid'] ? date("Y-m-d H:i:s",$masternode['lastpaid']) : '-' !!}</td>
                                        <td class="font-w600">
                                            {!! \Carbon\Carbon::now()->subSeconds($masternode['activetime'])->diffForHumans() !!}
                                        </td>
                                        <td class="font-w600">
                                            <a href="{!! route('address.show',$masternode['addr']) !!}">{!! $masternode['addr'] !!}</a>
                                        </td>
                                        <td class="font-w600">
                                            <a href="{!! route('tx.show',$masternode['txhash']).'?address='.$masternode['addr'] !!}">{!! str_limit($masternode['txhash'],50) !!}</a>
                                        </td>
                                        <td class="font-w600">
                                            {!! $masternode['outidx'] !!}
                                        </td>
                                        <td class="font-w600">
                                            {!! $masternode['version'] !!}
                                        </td>
                                        <td class="font-w600">
                                            {!! $masternode['status'] !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{--<div class="row">--}}
                            {{--<div>--}}
                                {{--<div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_1_paginate">--}}
                                    {{--{!! $masternodes->links() !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
