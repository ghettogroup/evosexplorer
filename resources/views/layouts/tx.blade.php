@extends('inc.main')

@section('content')
    <main id="main-container">
        <div class="content">
            @include('inc.top')

            <div class="row ">
                <div class="block">
                    <div class="block-header block-header-white">
                        <h3 class="block-title">TRANSACTION INFO</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-borderless table-vcenter">
                                <tbody>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">TXID:</th>
                                    <td class="font-w600">
                                        {!! $raw['txid'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Confirmations:</th>
                                    <td class="font-w600 text-danger">
                                        {{ $info['confirmations'] }}
                                    </td>
                                </tr>
                                {{--<tr class="mb-0">--}}
                                    {{--<th class="font-w600" scope="row">Block Value:</th>--}}
                                    {{--<td class="font-w600">--}}
                                        {{--125.0000 EVOS--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Block Hash:</th>
                                    <td class="font-w600 text-success">
                                        {!! $info['block_hash'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Block Height:</th>
                                    <td class="font-w600">
                                        {!! $info['block_height'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Timestamp:</th>
                                    <td class="font-w600">
                                        {!! $info['time'] !!}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="block block-rounded block-bordered col-md-6 col-sm-12">
                    <div class="block-header">
                        <h3 class="block-title">SENDING ADDRESSES</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-vcenter">
                                <thead>
                                    <tr>
                                        <th>ADDRESS</th>
                                        <th>VALUE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($transaction['out'] as $address=>$array)
                                    <tr>
                                        <td class="font-w600">
                                            <a href="{!! route("address.show", $address) !!}">{!! $address !!}</a>
                                        </td>
                                        <td class="font-w600 text-success">
                                            {!! formatMoney($array['value']) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="block block-rounded block-bordered col-md-6 col-sm-12">
                    <div class="block-header">
                        <h3 class="block-title">RECIPIENTS</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-vcenter">
                                <thead>
                                <tr>
                                    <th>ADDRESS</th>
                                    <th>AMOUNT</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transaction['in'] as $address=>$value)
                                    <tr>
                                        <td class="font-w600">
                                            <a href="{!! route('address.show', $address) !!}">{!! $address !!}</a>
                                        </td>
                                        <td class="font-w600 text-success">
                                            {!! formatMoney((float)$value) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
