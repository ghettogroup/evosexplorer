@extends('inc.main')

@section('content')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            @include('inc.top')

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">LATEST BLOCKS</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                        <table id="lastBlocks" class="table table-hover table-vcenter">
                            <thead>
                            <tr>
                                <th>HEIGHT</th>
                                <th>BLOCK HASH</th>
                                <th>VALUE</th>
                                <th>AGE</th>
                                <th>RECIPIENTS</th>
                                <th>CREATED</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6"><div class="timer"></div></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
