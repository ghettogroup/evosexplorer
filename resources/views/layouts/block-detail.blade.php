@extends('inc.main')

@section('content')
    <main id="main-container">
        <div class="content">
            @include('inc.top')

            <div class="row ">
                <div class="block">
                    <div class="block-header block-header-white">
                        <h3 class="block-title">BLOCK INFO</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                        <table class="table table-borderless table-vcenter">
                            <tbody>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Hash:</th>
                                    <td class="font-w600">
                                        {!! $block->hash !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Height:</th>
                                    <td class="font-w600 text-primary-dark">
                                        {!! $block->height !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Difficulty:</th>
                                    <td class="font-w600">
                                        {!! $summary['difficulty'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Confirmations:</th>
                                    <td class="font-w600 text-success">
                                        {!! $summary['confirmations'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Size (kB):</th>
                                    <td class="font-w600">
                                        {!! $summary['size'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Bits:</th>
                                    <td class="font-w600">
                                        {!! $summary['bits'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Nonce:</th>
                                    <td class="font-w600">
                                        {!! $summary['nonce'] !!}
                                    </td>
                                </tr>
                                <tr class="mb-0">
                                    <th class="font-w600" scope="row">Timestamp:</th>
                                    <td class="font-w600">
                                        {!! $summary['timestamp'] !!} UTC
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">BLOCK TRANSACTIONS</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-vcenter">
                                <thead>
                                <tr>
                                    <th>TRANSACTION ID</th>
                                    <th>RECEPIENTS</th>
                                    <th>TIME</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($block->raws as $trans)
                                    <tr>
                                        <td class="font-w600">
                                            <a href="/tx/{!! $trans->txid !!}">{!! $trans->txid !!}</a>
                                        </td>
                                        <td class="font-w600">
                                            {!! $trans->transactions->count() !!}
                                        </td>
                                        <td class="font-w600">
                                            {!! date("Y-m-d H:i:s", $trans->blocktime) !!} UTC
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
