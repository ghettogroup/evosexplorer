@extends('inc.main')

@section('content')
    <main id="main-container">
        <div class="content">
            @include('inc.top')

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">CONNECTIONS</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-vcenter">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th class="font-w700 text-center" width="16"></th>
                                        <th class="font-w700 text-center">Address</th>
                                        <th class="font-w700 text-center">Protocol</th>
                                        <th class="font-w700 text-center">SubVersion</th>
                                        <th class="font-w700 text-center">Country</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($connections as $connection)
                                    <tr>
                                        <td class="text-left"><img src="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.1/flags/4x3/{!! $connection['country'] !!}.svg" width="16" alt="" class="float-left"></td>
                                        <td class="text-left">{!! $connection['ip'] !!}</td>
                                        <td class="text-center">{!! $connection['version'] !!}</td>
                                        <td class="text-center">{!! $connection['protocol'] !!}</td>
                                        <td class="text-center">{!! $connection['name'] !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
