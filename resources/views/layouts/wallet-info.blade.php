@extends('inc.main')

@section('content')
    <main id="main-container">
        <div class="content">
            @include('inc.top')

            <div class="row ">
                <div class="block" style="width:100%;">
                    <div class="block-header block-header-white">
                        <h3 class="block-title">WALLET INFO</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive col-md-8 col-sm-12 float-left">
                            <table class="table table-borderless table-vcenter">
                                <tbody>
                                    <tr class="mb-0">
                                        <th class="font-w600" scope="row">Wallet Address:</th>
                                        <td class="font-w600">
                                            {!! $wallet->address !!}
                                        </td>
                                    </tr>
                                    <tr class="mb-0">
                                        <th class="font-w600" scope="row">Sent:</th>
                                        <td class="font-w600 text-primary-dark">
                                            {!! formatMoney($summary['out']) !!}
                                        </td>
                                    </tr>
                                    <tr class="mb-0">
                                        <th class="font-w600" scope="row">Received:</th>
                                        <td class="font-w600">
                                            {!! formatMoney($summary['in']) !!}
                                        </td>
                                    </tr>
                                    <tr class="mb-0">
                                        <th class="font-w600" scope="row">Balance:</th>
                                        <td class="font-w600 text-success">
                                            {!! formatMoney($summary['balance']) !!}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3 col-sm-12 m-1 float-left block block-bordered container-fluid">
                            <div class="block-content">
                                <img class="img-fluid p-3" src="{{ asset('images/qr_code.png') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">MOVEMENT</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-vcenter">
                                <thead>
                                <tr>
                                    <th>TRANSACTION ID</th>
                                    <th>AMOUNT</th>
                                    <th>TIME</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transactions as $key=>$transaction)
                                    <tr>
                                        <td class="font-w600">
                                            <a href="{!! url('tx/'.$key.'?address='.$wallet->address) !!}" >{!! $key !!}</a>
                                        </td>
                                        <td class="font-w600 text-success">
                                            {!! formatMoney(divide($transaction['value'])) !!}
                                        </td>
                                        <td class="font-w600">
                                            {!! date("Y-m-d H:i:s",$transaction['time']) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div>
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_1_paginate">
                                   {!! $transactionsArray->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
