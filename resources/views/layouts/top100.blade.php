@extends('inc.main')

@section('content')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            @include('inc.top')

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">LATEST BLOCKS</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                        <table class="table table-hover table-vcenter">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th>ADDRESS</th>
                                <th>TOTAL</th>
                                <th>%</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($wallets as $wallet)
                                <tr>
                                    <th class="text-center" scope="row">{!! $loop->iteration !!}</th>
                                    <td class="font-w600">
                                        <a href="{!! route('address.show',$wallet->address) !!}">{!! $wallet->address !!}</a>
                                    </td>
                                    <td class="font-w600">
                                        {!! formatMoney(divide($wallet->balance)) !!}
                                    </td>
                                    <td class="font-w600">
                                        {!! number_format(divide($wallet->balance)/$evos->info['supply']*100,2) !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
