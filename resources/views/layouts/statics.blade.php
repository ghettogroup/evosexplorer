@extends('inc.main')

@section('content')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            @include('inc.top')

            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">STATISTICS</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-6 mb-8">
                                <script src="https://widgets.coingecko.com/coingecko-coin-compare-chart-widget.js"></script>
                                <coingecko-coin-compare-chart-widget  coin-ids="acreage-coin,bitcoin,ethereum" currency="usd" locale="en" type="market_cap"></coingecko-coin-compare-chart-widget>
                            </div>
                            <div class="col-6 mb-8">
                                <script src="https://widgets.coingecko.com/coingecko-coin-list-widget.js"></script>
                                <coingecko-coin-list-widget  coin-ids="acreage-coin,bitcoin,ethereum,ripple,tether,litecoin,bitcoin-cash" currency="usd" locale="en"></coingecko-coin-list-widget>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
