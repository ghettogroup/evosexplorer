@extends('inc.main')

@section('content')
    <main id="main-container">
        <div class="content">
           @include('inc.top')
            <div>
                <div class="block block-rounded block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">MOVEMENTS</h3>
                    </div>
                    <div class="block-content">
                        <div class="table-responsive">
                        <table class="table table-hover table-vcenter">
                            <thead>
                            <tr>
                                <th>BLOCK HEIGHT</th>
                                <th>TRANSACTION HASH</th>
                                <th>AMOUNT</th>
                                <th>TIME</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td class="text-center" scope="row">{{ $transaction['block_height'] }}</td>
                                    <td class="font-w600">
                                        <a href="{!! route('tx.show', $transaction['txid']) !!}">{!! $transaction['txid'] !!}</a>
                                    </td>
                                    <td class="font-w600">
                                        <span class="badge badge-success">{!! $transaction['value'] !!}</span>
                                    </td>
                                    <td class="font-w600">
                                        {!! $transaction['created'] !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        <div class="row">
                            <div>
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_1_paginate">
                                    {!! $raws->links() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
