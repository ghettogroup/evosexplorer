@extends('inc.main')
@php($coininfo = getCoinInfo())
@section('content')
    <main id="main-container">
        <div class="content">
            @include('inc.top')

            <div class="row col-12">
                {{--<div class="col-8 offset-2 im">
                        <img class="img-fluid" style="background: #0f2735 !important;" src="{!! asset('images/largelogo.png') !!}">
                </div>--}}
                <div class="col-md-2 col-sm-12 float-left">
                    <a class="block mb-0" href="javascript:void(0)">
                        <div class="block-header">
                            <h3 class="block-title">Links</h3>
                        </div>
                        <div class="block-content">
                            <a href="https://acreagecoin.com/" target="_blank">Website</a><br />
                            <a href="https://bitcointalk.org/index.php?topic=4709055" target="_blank">BitcoinTalk</a><br />
                            <a href="https://github.com/acr-official" target="_blank">GitHub</a><br />
                            <a href="https://www.instagram.com/acreagecoin" target="_blank">Instagram</a><br />
                        </div>

                        <hr />

                        {{--<div class="block-header">
                            <h3 class="block-title">Exchanges</h3>
                        </div>
                        <div class="block-content">
                            <a href="https://wallet.crypto-bridge.org/market/BRIDGE.EVOS_BRIDGE.BTC" target="_blank">CryptoBridge</a><br />
                        </div>--}}
                    </a>
                </div>
                <div class="col-md-6 col-sm-12 float-left">
                    <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
                        <div class="block-header">
                            <h3 class="block-title">Estimated Earnings (COIN/BTC/USD)</h3>
                        </div>
                        <div class="block-content">
                            <table class="table table-borderless table-vcenter">
                                <tbody>
                                    <tr>
                                        <th class="font-w600" scope="row">DAILY</th>
                                        <td class="font-w600">
                                            {!! $coininfo['daily_evos'] .' ACREAGE' !!} / {!! $coininfo['daily_btc'] . ' BTC' !!} / {!! $coininfo['daily_dollar'] . ' USD' !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">WEEKLY</th>
                                        <td class="font-w600">
                                            {!! $coininfo['weekly_evos'] .' ACREAGE' !!} / {!! $coininfo['weekly_btc'] . ' BTC' !!} / {!! $coininfo['weekly_dollar'] . ' USD' !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">MONTHLY</th>
                                        <td class="font-w600">
                                            {!! $coininfo['monthly_evos'] .' ACREAGE' !!} / {!! $coininfo['monthly_btc'] . ' BTC' !!} / {!! $coininfo['monthly_dollar'] . ' USD' !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="font-w600" scope="row">YEARLY</th>
                                        <td class="font-w600">
                                            {!! $coininfo['annual_evos'] .' ACREAGE' !!} / {!! $coininfo['annual_btc'] . ' BTC' !!} / {!! $coininfo['annual_dollar'] . ' USD' !!}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-12 float-left">
                    <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
                        <div class="block-content">
                            <table class="table table-borderless table-vcenter">
                                <tbody>
                                <tr>
                                    <th class="font-w600" scope="row">{!! $coininfo['enabled_total'] !!}</th>
                                    <td class="font-w600">
                                        Active / Total Masternodes
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-w600" scope="row">20M ACREAGE</th>
                                    <td class="font-w600">
                                        Coin Supply (Total)
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-w600" scope="row">{!! $coininfo['circulating'] !!}</th>
                                    <td class="font-w600">
                                        Coin Supply (Circulating)
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-w600" scope="row">{!! $coininfo['market_cap_btc'] . ' BTC' !!}</th>
                                    <td class="font-w600">
                                        Market Cap BTC
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-w600" scope="row">{!! $coininfo['market_cap_usd'] . ' USD' !!}</th>
                                    <td class="font-w600">
                                        Market Cap USD
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-w600" scope="row">{!! $coininfo['locked'] . ' ACREAGE' !!}</th>
                                    <td class="font-w600">
                                        Coins Locked
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-w600" scope="row">{!! '$ ' . $coininfo['masternode_worth'] !!}</th>
                                    <td class="font-w600">
                                        Masternode Worth
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- END Page Content -->

    </main>
@endsection
