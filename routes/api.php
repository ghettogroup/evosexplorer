<?php

Route::get('lastBlocks','BlockController@lastBlocks');
Route::get('lastTransactions','TxController@lastTransactions');
Route::get('mainStats', 'OverviewController@mainStats');
Route::get('rpc/getinfo','OverviewController@getinfo');
Route::get('rpc/getnetworkhashps','OverviewController@getnetworkhashps');
Route::get('rpc/masternode,count','OverviewController@getMasterNodeCount');
Route::get('rpc/supply','OverviewController@supply');
Route::get('rpc/totalWallets','OverviewController@totalWallets');
