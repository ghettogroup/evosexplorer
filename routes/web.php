<?php

Route::get('/', 'OverviewController@index');

Route::get('/movements', 'TxController@index')->name('movements');

Route::get('/tx/{id}', 'TxController@show')->name('tx.show');

Route::get('/top100', 'WalletController@index');

Route::get('/wallet-info/{id}', 'WalletController@show')->name('address.show');

Route::get('/masternode', 'MasternodeController@index');

Route::get('/connections', 'ConnectionController@index');

Route::get('/block/{id}', 'BlockController@show')->name('block.show');

Route::get('/top-stats', 'OverviewController@mainStats');

Route::post('/search', 'OverviewController@search');

Route::post('/search/delete', 'OverviewController@searchDelete');

Route::get('/statics', function () {
    return view('layouts.statics');
});

Route::get('/coin-info', function () {
    return view('layouts.coininfo');
});



