<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletsTable extends Migration {

	public function up()
	{
		Schema::create('wallets', function(Blueprint $table) {
			$table->increments('id');
			$table->string('address');
            $table->bigInteger('balance');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('wallets');
	}
}