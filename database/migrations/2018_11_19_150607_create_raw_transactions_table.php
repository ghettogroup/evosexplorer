<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRawTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('raw_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('block_id')->unsigned()->index();
			$table->text('hex');
			$table->string('txid');
			$table->integer('version');
			$table->integer('locktime');
			$table->longText('vin');
			$table->longText('vout');
			$table->string('blockhash');
			$table->integer('time');
			$table->integer('blocktime');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('raw_transactions');
	}
}