<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('raw_transactions', function(Blueprint $table) {
			$table->foreign('block_id')->references('id')->on('blocks')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('transactions', function(Blueprint $table) {
			$table->foreign('transaction_id')->references('id')->on('raw_transactions')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('transactions', function(Blueprint $table) {
			$table->foreign('wallet_id')->references('id')->on('wallets')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('stats', function(Blueprint $table) {
			$table->foreign('block_id')->references('id')->on('blocks')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('transactions', function(Blueprint $table) {
            $table->foreign('block_id')->references('id')->on('blocks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	public function down()
	{
		Schema::table('raw_transactions', function(Blueprint $table) {
			$table->dropForeign('raw_transactions_block_id_foreign');
		});
		Schema::table('transactions', function(Blueprint $table) {
			$table->dropForeign('transactions_transaction_id_foreign');
		});
		Schema::table('transactions', function(Blueprint $table) {
			$table->dropForeign('transactions_wallet_id_foreign');
		});
		Schema::table('stats', function(Blueprint $table) {
			$table->dropForeign('stats_block_id_foreign');
		});
        Schema::table('transactions', function(Blueprint $table) {
            $table->dropForeign('transactions_block_id_foreign');
        });
	}
}