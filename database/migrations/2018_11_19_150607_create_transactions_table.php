<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('transaction_id')->unsigned()->index();
			$table->integer('block_id')->unsigned()->index();
			$table->string('txid');
			$table->integer('wallet_id')->unsigned()->index()->nullable();
			$table->string('way');
			$table->string('address')->nullable();
			$table->bigInteger('value');
			$table->integer('n');
            $table->string('referer_txid')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('transactions');
	}
}