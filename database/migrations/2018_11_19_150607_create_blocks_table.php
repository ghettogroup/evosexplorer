<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration {

	public function up()
	{
		Schema::create('blocks', function(Blueprint $table) {
			$table->increments('id');
			$table->string('hash');
			$table->integer('size');
			$table->integer('height');
			$table->integer('version');
			$table->string('merkleroot');
			$table->text('tx');
			$table->integer('time');
			$table->string('nonce');
			$table->string('bits');
			$table->string('difficulty');
			$table->string('chainwork');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('blocks');
	}
}
