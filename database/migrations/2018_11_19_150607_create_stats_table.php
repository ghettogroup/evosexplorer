<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatsTable extends Migration {

	public function up()
	{
		Schema::create('stats', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('block_id')->unsigned()->index();
			$table->string('network_hash');
			$table->string('difficulty');
			$table->integer('masternodes');
			$table->string('price');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('stats');
	}
}
