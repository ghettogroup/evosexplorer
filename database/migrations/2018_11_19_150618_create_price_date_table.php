<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreatePriceDateTable extends Migration {

	public function up()
	{
		Schema::create('price_date', function(Blueprint $table) {
		    $table->increments('id');
			$table->decimal('price');
			$table->timestamp('date');
		});

	}

	public function down()
	{
		Schema::drop('price_date');

	}
}
